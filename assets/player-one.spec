# Player spec is a file that specify which sprites the game must use and how
# The file must contain theses attributes :
#
# Positions are given in pixels
#
# - FILENAME <filename>
# - MOVE-LEFT <pos x>,<pos y> <width>,<height> <offset x>,<offset y> <center x>,<center y> <number of sprites>
# - MOVE-RIGHT <pos x> <pos y> <width> <height> <offset x> <offset y> <center x>,<center y> <number of sprites>
# - JUMP-LEFT <pos x> <pos y> <width> <height> <offset x> <offset y> <center x>,<center y> <number of sprites>
# - JUMP-RIGHT <pos x> <pos y> <width> <height> <offset x> <offset y> <center x>,<center y> <number of sprites>
# - ATTACK-LEFT <pos x> <pos y> <width> <height> <offset x> <offset y> <center x>,<center y> <number of sprites>
# - ATTACK-RIGHT <pos x> <pos y> <width> <height> <offset x> <offset y> <center x>,<center y> <number of sprites>

FILENAME playerone.png
MOVE-LEFT 0,582 62,62 0,0 31,31 9
MOVE-RIGHT 0,706 62,62 0,0 31,31 9
JUMP-LEFT 0,582 62,62 0,0 31,31 9
JUMP-RIGHT 0,706 62,62 0,0 31,31 9
ATTACK-LEFT 0,1488 186,186 0,0 93,93 6
ATTACK-RIGHT 0,1860 186,186 0,0 93,93 6