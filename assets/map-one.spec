# Map specs is a file that specify which sprites the game must use and how
#
# Positions are given in pixels
#
# The file must contain theses attributes :
#
# - LAYER <layer index>
#       This attribute creates a layer and uses it as default for the following attributes.
#       (Making the <layer index> parameter optional)
#
# - FILENAME <filename> | <layer index> <filename>
#       This attribute tels which file to use to represent which parallax layer.
#       Parameters :
#           - filename: This parameter specify the path to the image to open as layer.
#                       The path is absolute from the asset folder.
#           - layer index: is optional and will take by default the last LAYER value or 0 if none is previously set.
#
# - SPEED_OFFSET <offset> | <layer index> <offset>
#       This attribute is optional and tels by how much the layer speed is decreased regarding the previous layer.
#       Parameters :
#           - offset: Must be an integer from 0 to 100. 0 means no displacement. 100 means same speed as the foreground. (100 by default).
#           - layer index: is optional and will take by default the last LAYER value or 0 if none is previously set.
#
# - POSITION_OFFSET <x>,<y> | <layer index> <x>,<y>
#       This attribute is optional and tels if the layer must be displaced.
#       By example if you have two layers :
#
#         window
#       *-----------------------------------------------*
#       |   o---------------------------------------o   |
#       |   o         layer 1                       o   |
#       |   o                                       o   |
#       |   o         i------i                      o   |
#       |   i--------i        i---------------------i   |
#       |   i          layer 0                      i   |
#       |   i_______________________________________i   |
#       *-----------------------------------------------*
#
#       The 'layer 1' must be displayed above the 'layer 0'.
#       The position offset y will be equal to the height of the 'layer 0'
#       Parameters :
#           - <x>,<y> : The offset values. Values must be given as pixels.
#           - layer index: is optional and will take by default the last LAYER value or 0 if none is previously set.

LAYER 0
FILENAME background.jpg