/*
** free_n.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include <stdlib.h>

/*
**	free_2 : Libère le tableau à double entrée "tab"
**
**	Arguments :
**		- tab : Le tableau à libérer
**
**	Valeur de retour : (void)
**
**	Alias : Pas d'allias
*/

void	free_2(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      free(tab[i]);
      i++;
    }
  free(tab);
  tab = NULL;
  return ;
}
