/*
** tablen.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

/*
**	tablen : Mesure la longueur d'un tableau
**
**	Arguments :
**		- tab  : La chaîne à mesurer
**
**	Valeur de retour :
**		- La longueur du tableau
**
**	Alias : Pas d'allias
*/

int	tablen(char **tab)
{
  int	i;

  i = 0;
  while (tab && tab[i])
    i++;
  return (i);
}
