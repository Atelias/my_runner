/*
** str_ends.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  09:56 21/12/2020 Thomas Lombard
** Last update 09:56 21/12/2020 Thomas Lombard
*/

#include "lib/lib.h"

/*
**	str_ends_with : détermine si une chaine est terminée par une autre
**
**	Arguments :
**		- str    : La chaîne dans laquelle chercher
**		- search : La chaîne à chercher
**
**	Valeur de retour :
**		- 1 si search termine str
**		- 0 autrement
**
**	Alias : Pas d'allias
*/
int str_ends_with(const char *str, const char *search) {
	int strl    = 0;
	int searchl = 0;

	strl    = my_strlen(str) - 1;
	searchl = my_strlen(search) - 1;
	if (strl < searchl) {
		return 0;
	}
	if (my_strcmp(&str[strl - searchl], search) == 0)
		return 1;
	return 0;
}

/*
**	my_str_trim_ends : efface la fin d'une chaine si elle correspond à la chaine de recherche
**
**	Arguments :
**		- str    : La chaîne dans laquelle effacer
**		- search : La chaîne à chercher
**
**	Valeur de retour :
**		- 'str' dans tous les cas
**
**	Alias : Pas d'allias
*/
char *str_trim_end(char *str, const char *search) {
	int searchl;
	int strl;

	searchl = my_strlen(search);
	strl    = my_strlen(str);
	if (str_ends_with(str, search))
		my_memset(&str[strl - searchl], 0, searchl);
	return str;
}