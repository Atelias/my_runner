/*
** printf__calc.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include "m_printf.h"

int			printf__calc(UNUSED const char		*format,
				     UNUSED va_list		ap,
				     UNUSED t_printf_out	*out)
{
  char			*tmp;

  while (format && *format)
    {
      if (*format == '%')
	{
	  tmp = (char *)format + 1;
	  if (printf__parse(&tmp, ap, out))
	    format = tmp;
	  else
	    printf__print(*format, out);
	}
      else
	printf__print(*format, out);
      format++;
    }
  return (out->wrote);
}
