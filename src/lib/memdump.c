//
// Created by Thomas Lombard on 1/1/21.
//

typedef struct s_memdump_opt {
    size_t hex_len;
    char hex_len_space_pad;
    size_t group_bits;
    size_t bits_per_line;
} t_memdump_opt;

extern const t_memdump_opt MEMDUMP_XXD;
extern const t_memdump_opt MEMDUMP_DEFAULT;
extern const t_memdump_opt MEMDUMP_EASY_READ;
extern const t_memdump_opt MEMDUMP_OBJDUMP;

const t_memdump_opt MEMDUMP_XXD = {
        .hex_len = 8,
        .hex_len_space_pad = 0,
        .group_bits = 2,
        .bits_per_line = 16,
};

const t_memdump_opt MEMDUMP_EASY_READ = {
        .hex_len = 8,
        .hex_len_space_pad = 1,
        .group_bits = 2,
        .bits_per_line = 16,
};

const t_memdump_opt MEMDUMP_DEFAULT = {
        .hex_len = 8,
        .hex_len_space_pad = 0,
        .group_bits = 2,
        .bits_per_line = 16,
};

void memdump_make_prefix(char prefix[42], t_memdump_opt opt) {
    if (opt.hex_len_space_pad) {
        sprintf(prefix, "%% %lulx | ", opt.hex_len);
    } else {
        sprintf(prefix, "%%.%lulx | ", opt.hex_len);
    }
}

void print_hex(const void *mem, size_t length, size_t offset, t_memdump_opt opts) {
    register const char *p = mem;
    register size_t line_length;

    line_length = 0;
    while (line_length < opts.bits_per_line) {
        if ((line_length + 1 + offset) > length)
            break;
        if (((line_length + 1) % opts.group_bits))
            printf("%02hhx", p[line_length + offset]);
        else
            printf("%02hhx ", p[line_length + offset]);
        ++line_length;
    }
    while (line_length < opts.bits_per_line) {
        if (!((++line_length) % opts.group_bits))
            printf("   ");
        else
            printf("  ");
    }
}

static void print_displayable(const void *mem, size_t length, size_t offset, t_memdump_opt opt) {
    register const char *p = mem;
    register size_t line_length;

    line_length = 0;
    printf("| ");
    while (line_length < opt.bits_per_line) {
        if ((line_length + 1 + offset) > length)
            break;
        if (isprint(p[line_length + offset]))
            printf("%c", (char)p[line_length + offset]);
        else
            printf(".");
        ++line_length;
    }
}

void memdump_opt(const void *mem, size_t length, t_memdump_opt opts) {
    char prefix[42] = {0};
    register size_t offset = 0;

    memdump_make_prefix(prefix, opts);
    while (length > offset) {
        printf(prefix, offset);
        print_hex(mem, length, offset, opts);
        print_displayable(mem, length, offset, opts);
        puts("");
        offset += opts.bits_per_line;
        if (length < opts.bits_per_line) {
            break;
        }
    }
}

void memdump(void *mem, size_t len) {
    memdump_opt(mem, len, MEMDUMP_DEFAULT);
}
