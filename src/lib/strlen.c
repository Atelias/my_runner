/*
** strlen.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

/*
**	my_strlen : man strlen
**
**	Arguments :
**		- str  : La chaîne à mesurer
**
**	Valeur de retour :
**		- La longueur de la chaîne
**
**	Alias :
**		- m_strl(str) -> my_strlen(str)
*/
int			my_strlen(const char *str)
{
  register const char	*s;

  s = str;
  while (s && *s)
    s++;
  return (s - str);
}
