/*
** measurement.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include "lib/list.h"

/*
**	list_length :
**		Renvoie la longueur d'une liste, quelque soit son type
**		!! Ne fonctionne seulement si les deux premiers items de
**		   la structure chaînée sont un pointeur next puis prev.
**
**	Arguments :
**		- l    : Un des noeuds de la liste (*)
**
**	Valeur de retour :
**		- Le nombre de noeuds dans la liste
**
**	Alias :
**		Pas d'alias
*/

size_t		list_length(void *l)
{
  t_list	*list;
  size_t	ret;

  list = list_get_first(l);
  ret = 0;
  while (list)
    {
      list = list->next;
      ++ret;
    }
  return (ret);
}
