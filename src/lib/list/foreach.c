/*
** foreach.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  11:22 21/12/2020 Thomas Lombard
** Last update 11:22 21/12/2020 Thomas Lombard
*/

#include "lib/list.h"

/*
**	list_get_first :
**		Appelle la fonction call sur chacun des éléments de la liste.
**		!! Ne fonctionne seulement si les deux premiers items de
**		   la structure chaînée sont un pointeur next puis prev.
**
**	Arguments :
**		- l    : Un des noeuds de la liste (*)
**		- call : La fonction à appeller.
**
**	Valeur de retour :
**		- Un pointeur vers le premier noeud de la liste
**
**	Alias :
**		Pas d'alias
*/
void		*list_foreach(void	*l, void(*call)(void *node))
{
	t_list	*list;

	l = list_get_first(l);
	list = l;
	while (list) {
		call(l);
		list = list->next;
	}
	return (l);
}
