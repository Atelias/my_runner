/*
** memcpy.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include "lib/lib.h"

/*
**	my_memcpy : Copie 'n' octets de 'src' dans 'dest'
**
**	Arguments :
**		- dest   : Le pointeur où écrire
**		- src    : Le pointeur à utiliser
**		- n      : La taille de la zonne à écrire
**
**	Valeur de retour :
**		- Le même pointeur que "dest"
**
**	Alias : Pas d'allias
*/
void		*my_memcpy(void *dest, const void *src, size_t n)
{
  const char	*s;
  char		*d;

  d = dest;
  s = (char const *)src;
  while (n--)
    *d++ = *s++;
  return (dest);
}
