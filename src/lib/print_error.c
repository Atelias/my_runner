/*
** print_error.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include <stdlib.h>
#include "lib/lib.h"

/*
**	print_error : Affiche une erreur avec un format particlier sur
**		      la sortie d'erreur avec m_vdprintf.
**		      Plus d'infos : man vdprintf
**
**	Arguments :
**		- error : Le format de l'erreur
**		- ...   : Les varables de l'erreur
**
**	Valeur de retour :
**		- Toujours NULL
**
**	Alias : Pas d'allias
*/

void	*print_error(const char *error, ...)
{
  va_list(ap);

  va_start(ap, error);
  m_vdprintf(2, (char*)error, ap);
  va_end(ap);
  return (NULL);
}
