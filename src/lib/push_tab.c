/*
** push_tab.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include "lib/lib.h"

/*
**	push_tab : Ajoute une case à "tab". Crée le tableau si "tab" est NULL
**
**	Arguments :
**		- tab    : Le tableau d'entrée
**		- line   : L'entrée à ajouter
**
**	Valeur de retour :
**		- Le nouveau tableau
**
**	Alias : Pas d'allias
*/

char		**push_tab(char **tab, char *line)
{
  char		**ret;
  int		i;

  i = tablen(tab) + 2;
  if (!(ret = xcalloc(i, sizeof(char *))))
    return (NULL);
  i = -1;
  while (tab && tab[++i])
    ret[i] = tab[i];
  i = (i == -1) ? 0 : i;
  if (!(ret[i] = xstrdup(line)))
    return (NULL);
  free(tab);
  return (ret);
}
