/*
** nbrlen.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

/*
**	nbrlen : Donne la longueur d'un nombre s'il devait être affiché
**
**	Arguments :
**		- nb    : Le nombre à mesurer
**
**	Valeur de retour :
**		- La longueur mesurée
**
**	Alias : Pas d'allias
*/

int	nbrlen(long nbr)
{
  int	ret;

  ret = 0;
  while (nbr > 0 && nbr / 10 > 10)
    {
      nbr /= 10;
      ++ret;
    }
  return (ret);
}
