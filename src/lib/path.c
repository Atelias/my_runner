/*
** path.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  14:09 23/12/2020 Thomas Lombard
** Last update 14:09 23/12/2020 Thomas Lombard
*/

#include <stdarg.h>

#include "lib/lib.h"

char *path_join(char *path, ...) {
	va_list ap;
	va_list copy;
	int len = 0;
	char *ret;
	char *arg;

	va_start(ap, path);
	va_copy(copy, ap);
	len = m_strl(path) + (str_ends_with(path, PATH_SEPARATOR) ? 0 : 1);
	while ((arg = va_arg(copy, char *)))
		len += m_strl(ret) + (str_ends_with(arg, PATH_SEPARATOR) ? 0 : 1);
	ret = xcalloc(len + 1, 1);
	my_strcpy(ret, path);
	while ((arg = va_arg(ap, char *))) {
		if (!str_ends_with(ret, PATH_SEPARATOR))
			my_strcat(ret, PATH_SEPARATOR);
		my_strcat(ret, arg);
	}
	va_end(copy);
	va_end(ap);
	return ret;
}
