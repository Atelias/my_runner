/*
** str_ends.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  09:56 21/12/2020 Thomas Lombard
** Last update 09:56 21/12/2020 Thomas Lombard
*/

#include "lib/lib.h"

/*
**	str_ends_with : détermine si une chaine commence par une autre
**
**	Arguments :
**		- str    : La chaîne dans laquelle chercher
**		- search : La chaîne à chercher
**
**	Valeur de retour :
**		- 1 si search commence str
**		- 0 autrement
**
**	Alias : Pas d'allias
*/
int str_starts_with(const char *str, const char *search) {
	if (my_strncmp(str, search, my_strlen(search)) == 0)
		return 1;
	return 0;
}

/*
**	my_str_trim_ends : efface le début d'une chaine si elle correspond à la chaine de recherche
**
**	Arguments :
**		- str    : La chaîne dans laquelle effacer
**		- search : La chaîne à chercher
**
**	Valeur de retour :
**		- 'str' dans tous les cas
**
**	Alias : Pas d'allias
*/
char *str_trim_start(char *str, const char *search) {
	int searchl;

	searchl = my_strlen(search);
	if (str_starts_with(str, search))
		my_memset(str, 0, searchl);
	return &str[searchl];
}