/*
** print_spritesheet.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:36 23/12/2020 Thomas Lombard
** Last update 22:36 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_spritesheet_slice_spec(t_spritesheet_slice_specs *slice_specs) {
	m_printf("\t\t\tPosition  : %ux %uy\n", slice_specs->pos.x, slice_specs->pos.y);
	m_printf("\t\t\tSize      : %ux %uy\n", slice_specs->size.x, slice_specs->size.y);
	m_printf("\t\t\tCenter    : %ux %uy\n", slice_specs->center.x, slice_specs->center.y);
	m_printf("\t\t\tOffset    : %ux %uy\n", slice_specs->offset.x, slice_specs->offset.y);
	m_printf("\t\t\tNb slices : %u\n", slice_specs->n_slices);
}

void print_spritesheet_assets_specs_node(void *node) {
	t_spritesheet_assets_specs_map *n = node;
	m_printf("\t\tSpritesheet key %s\n", n->spritesheet_key);
	m_printf("\t\tMOVE_LEFT_SPECS: \n");
	print_spritesheet_slice_spec(&n->move_left);
	m_printf("\t\tMOVE_RIGHT_SPECS: \n");
	print_spritesheet_slice_spec(&n->move_right);
	m_printf("\t\tATTACK_LEFT_SPECS: \n");
	print_spritesheet_slice_spec(&n->attack_left);
	m_printf("\t\tATTACK_RIGHT_SPECS: \n");
	print_spritesheet_slice_spec(&n->attack_right);
	m_printf("\t\tJUMP_LEFT_SPECS: \n");
	print_spritesheet_slice_spec(&n->jump_left);
	m_printf("\t\tJUMP_RIGHT_SPECS: \n");
	print_spritesheet_slice_spec(&n->jump_right);
}

void print_spritesheet_assets_specs_map(t_spritesheet_assets_specs_map *map) {
	m_printf("\tSpritesheet assets specs map\n");
	list_foreach(map, print_spritesheet_assets_specs_node);
}
