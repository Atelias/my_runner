/*
** print_world_parallax_specs_map.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:37 23/12/2020 Thomas Lombard
** Last update 22:37 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_world_parallax_specs_map_node(void *node) {
	t_world_parallax_specs_map *n = node;
	t_world_parallax_specs     *s;
	int                        i  = 0;

	m_printf("\tSpecification key: %s\n", n->key);
	m_printf("\tLayers:\n");
	while (n->layers && n->layers[i]) {
		s = n->layers[i];
		m_printf("\t\tLayer: 		  %d\n", s->layer_index);
		m_printf("\t\tSpeed offset: 	  %d\n", s->speed_offset);
		m_printf("\t\tPosition offset: %ux %uy\n", s->position_offset.x, s->position_offset.y);
		m_printf("\t\tSprite key:      %s\n", s->sprite_key);
		++i;
	}
}

void print_world_parallax_specs_map(t_world_parallax_specs_map *map) {
	m_printf("\tWorld parallax specs map:\n");
	list_foreach(map, print_world_parallax_specs_map_node);
};