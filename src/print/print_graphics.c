/*
** print_graphics.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:34 23/12/2020 Thomas Lombard
** Last update 22:34 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_graphics(const t_graphics *graphics) {
	m_printf("Program graphics loaded: \n");
	print_texture_map(graphics->texture_map);
	print_spritesheet_assets_specs_map(graphics->sasm);
	print_world_parallax_specs_map(graphics->wpsm);
}
