/*
** print.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:27 23/12/2020 Thomas Lombard
** Last update 22:27 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_core(const t_core *core) {
	print_config(&core->config);
	print_graphics(&core->graphics);
}
