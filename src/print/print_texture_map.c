/*
** print_texture_map.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:35 23/12/2020 Thomas Lombard
** Last update 22:35 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_texture_map_node(void *node) {
	m_printf("\t\tTexture key: %s\n", ((t_texture_map *) node)->key);
}

void print_texture_map(t_texture_map *map) {
	m_printf("\tLoaded textures Map:\n");
	list_foreach(map, print_texture_map_node);
}
