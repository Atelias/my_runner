/*
** print_config.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:36 23/12/2020 Thomas Lombard
** Last update 22:36 23/12/2020 Thomas Lombard
*/

#include "header.h"

void print_config(const t_config *config) {
	m_printf("Program configuration: \n");
	m_printf("\tAssets directory: %s\n", config->asset_directory);
	m_printf("\tGame resolution: %ux%u\n", config->size.x, config->size.y);
}
