/*
** set_fnc_tab.c for clusterv2 in /clusterv2/src/args
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  Sun May 21 09:54:13 2017 Thomas Lombard
** Last update Sun May 28 10:41:49 2017 Thomas Lombard
*/

#include <limits.h>
#include <getopt.h>
#include "header.h"

static int opt_help(t_core *core, UNUSED void *data) {
    m_printf("SYNOPSIS :\n"
             "\t%s\n"
             "DESCRIPTION :\n"
             "\nAssets Specs:\n"
			 "    --asset-location <path/to/assets>\n"
             "\nWindow Specs:\n"
             "    --win-size <width>x<height>\n"
             "\tDetermine the size of the display.\n"
             "\tValue infos :\n"
             "\t    Must be one of following values :\n"
             "\t\t- 1280x720  (720p)\n"
             "\t\t- 1920x1080 (1080p)\n"
             "\t\t- 2560x1440 (1440p / WQHD / 2k)\n"
             "\t\t- 3840x2160 (2160p / 4K UHD)\n"
             "\n",
             core->args.av[0]);
    return (-1);
}

void set_fnc_tab(t_fnc fnc[NB_FLAGS]) {
    fnc[0].flag = 'h';
    fnc[0].fnc = opt_help;
    set_fnc_tab_window_size(fnc, 1);
}
