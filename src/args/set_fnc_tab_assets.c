/*
** set_fnc_tab_assets.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  00:47 21/12/2020 Thomas Lombard
** Last update 00:47 21/12/2020 Thomas Lombard
*/
#include "header.h"

static int assets_location(t_core *core, void *data) {
	struct stat st;

	if (data && !stat(data, &st)) {
		if (!S_ISDIR(st.st_mode)) {
			arg_error(core->args.av, data);
			m_dprintf(2, "%s given path does not lead to a directory\n", data);
			return e_badArg;
		}
		core->config.asset_directory = data;
		return e_noErr;
	}

	arg_error(core->args.av, data);
	m_dprintf(2, "%s is not a valid directory\n", data);
	return e_badArg;
}

void set_fnc_tab_assets(t_fnc fnc[NB_FLAGS], int i) {
	fnc[i++] = (t_fnc) {'a', assets_location};
}
