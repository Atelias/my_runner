/*
** EPITECH PROJECT, 2021
** arcade
** File description:
** Created by lombar_e,
*/

#include <errno.h>
#include <SFML/System.h>

#include "header.h"

/*
 * Returns 1 if the resolution is allowed, 0 otherwise
 *
 * The resolution must be one of theses :
 *  - 16:9
 *  	- 1280x720  (720p)
 *  	- 1920x1080 (1080p)
 *  	- 2560x1440 (1440p / WQHD / 2k)
 *  	- 3840x2160 (2160p / 4K UHD)
 */
static int is_resolution_allowed(int x, int y) {
	if ((x == 1280 && y == 720) ||
		(x == 1920 && y == 1080) ||
		(x == 2560 && y == 1440) ||
		(x == 3840 && y == 2160))
		return 1;
	return 0;
}

static int parse_opt_window_size(t_core *core, void *data) {
	t_vector *w_size = &core->config.size;
	char     **opts;

	opts = explode(data, 'x');
	if (opts == NULL)
		return e_alloc;
	if (tablen(opts) != 2)
		return e_badArg;
	w_size->x = strtoul(opts[0], NULL, 10);
	if (w_size->x == 0 && errno != 0)
		return e_errno;
	w_size->y = strtoul(opts[1], NULL, 10);
	if (w_size->x == 0 && errno != 0)
		return e_errno;
	if (!is_resolution_allowed(w_size->x, w_size->y))
		return e_badArg;
	return e_noErr;
}

static int opt_window_size(t_core *core, void *data) {
	int err = 0;

	if (data)
		err = parse_opt_window_size(core, data);
	if (err == 0 || err == e_alloc)
		return err;
	arg_error(core->args.av, data);
	m_dprintf(2, "%s", data);
	switch (err) {
		case e_badArg:
			m_dprintf(2, " is not a valid resolution");
			break;
		case e_errno:
			m_dprintf(2, ": %s", strerror(errno));
			break;
		default:
			m_dprintf(2, ": an unknown error occurred");
			break;
	}
	m_dprintf(2, "See --help for more information\n");
	return e_badArg;
}

void set_fnc_tab_window_size(t_fnc fnc[NB_FLAGS], int i) {
	fnc[i++] = (t_fnc) {'s', opt_window_size};
	set_fnc_tab_assets(fnc, i);
}
