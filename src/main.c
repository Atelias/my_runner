/*
** main.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  17:50 20/12/2020 Thomas Lombard
** Last update 17:50 20/12/2020 Thomas Lombard
*/

#include <stdlib.h>
#include <errno.h>
#include "header.h"

int main(int ac, char **av, char **ev) {
    t_core core = {0};
    enum e_error ret;

    if (getenv("DISPLAY") == NULL)
        return 84;
    if ((ret = core_init(&core, ac, av, ev)) != e_noErr) {
    	if (ret == e_errno)
    		m_dprintf(2, "An error occurred: %s\n", strerror(errno));
    	return ret;
    }

    return 0;
}