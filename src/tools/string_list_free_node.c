/*
** string_list_free_node.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  18:39 23/12/2020 Thomas Lombard
** Last update 18:39 23/12/2020 Thomas Lombard
*/

#include "header.h"

void string_list_free_node(void *node_to_free) {
	t_string_list *node = node_to_free;

	free(node->str);
	return;
}