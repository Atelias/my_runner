/*
** sf_vector_from_string.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  13:49 21/12/2020 Thomas Lombard
** Last update 13:49 21/12/2020 Thomas Lombard
*/

#include "header.h"

t_vector t_vector_from_string(char *str) {
	char     **split = explode(str, ',');
	t_vector ret     = {0, 0};

	if (tablen(split) != 2)
		return ret;
	ret.x = strtoul(split[0], NULL, 10);
	ret.y = strtoul(split[1], NULL, 10);
	free_2(split);
	return ret;
}