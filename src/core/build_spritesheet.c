/*
** build_spritesheet.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  12:29 21/12/2020 Thomas Lombard
** Last update 12:29 21/12/2020 Thomas Lombard
*/

#include <SFML/System/Vector2.h>

#include "header.h"

enum e_error build_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics) {
	enum e_error err;

	if ((err = build_character_spritesheets(files, config, graphics)) != e_noErr)
		return err;
	if ((err = build_map_spritesheets(files, config, graphics)) != e_noErr)
		return err;
	return e_noErr;
}
