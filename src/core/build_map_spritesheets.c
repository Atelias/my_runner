/*
** build_map_spritesheets.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  18:45 23/12/2020 Thomas Lombard
** Last update 18:45 23/12/2020 Thomas Lombard
*/

#include "header.h"

static void handle_map_line(char **args, t_world_parallax_specs_map *world) {
	t_world_parallax_specs *current = list_get_last(world->layers);

	if (str_starts_with(args[0], SPEC_LAYER)) {
		strtoul(args[1], NULL, 10);
	}
}

static enum e_error build_map_spritesheet(char *filepath, t_world_parallax_specs_map *world) {
	int  fp;
	char *line;
	char **args;

	if ((fp      = open(filepath, O_RDONLY)) <= -1)
		return e_errno;
	while ((line = get_next_line(fp)) != NULL) {
		if (str_starts_with(line, "#")) {
			free(line);
			continue;
		} else if (my_strlen(line) == 0)
			continue;
		if (!(args = explode(line, ' ')))
			return e_alloc;
		handle_map_line(args, world);
		free_2(args);
	}
	return e_noErr;
}

int filter_select_world(void *w) {
	return str_starts_with(((t_string_list *) (w))->str, "map-");
}

enum e_error build_map_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics) {
	t_world_parallax_specs_map *wpsm_node = NULL;
	enum e_error               err;
	char                       *filepath;
	t_string_list              *list      = files;

	while (files) {
		if (str_starts_with(files->str, "map-")) {
			if (!(wpsm_node = xcalloc(1, sizeof(typeof(*wpsm_node)))) ||
				!(filepath = path_join(config->asset_directory, files->str, NULL)))
				return e_alloc;
			if ((err       = build_map_spritesheet(filepath, wpsm_node)) != e_noErr)
				return err;
			graphics->sasm = list_push(graphics->sasm, wpsm_node);
		}
		files = files->next;
	}
	list_filter(files, filter_select_world, string_list_free_node);
	return e_noErr;
}