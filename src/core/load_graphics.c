/*
** load_assets.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  09:11 21/12/2020 Thomas Lombard
** Last update 09:11 21/12/2020 Thomas Lombard
*/

#include <dirent.h>

#include "header.h"

static t_string_list *get_asset_list(t_config *config) {
	struct dirent *dirent    = NULL;
	t_string_list *ret       = NULL;
	t_string_list *node      = NULL;
	DIR           *asset_dir = opendir(config->asset_directory);

	if (asset_dir != NULL) {
		while ((dirent = readdir(asset_dir)) != NULL) {
			if (str_ends_with(dirent->d_name, ".spec")) {
				if (!(node = xcalloc(1, sizeof(t_string_list))))
					return NULL;
				if (!(node->str = xstrdup(dirent->d_name)))
					return NULL;
				ret = list_push(ret, node);
			}
		}
	}
	return ret;
}

enum e_error load_graphics(t_config *config, t_graphics *graphics) {
	t_string_list *files = get_asset_list(config);
	enum e_error err;

	if (files == NULL)
		return e_errno;
	if ((err = build_spritesheets(files, config, graphics)) != e_noErr)
		return err;
	return e_noErr;
}