/*
** core.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  23:26 20/12/2020 Thomas Lombard
** Last update 23:26 20/12/2020 Thomas Lombard
*/

#include "config.h"
#include "header.h"

static enum e_error init_config(t_config *config) {
	config->asset_directory = xstrdup(DEFAULT_ASSET_DIRECTORY);
	if (!config->asset_directory)
		return e_alloc;
	config->size.x = DEFAULT_SCREEN_WIDTH;
	config->size.y = DEFAULT_SCREEN_HEIGHT;

	return e_noErr;
}

enum e_error core_init(t_core *core, int ac, char **av, char **ev) {
	core->args.ac = ac;
	core->args.av = av;
	core->args.ev = ev;
	enum e_error ret;

	if ((ret = init_config(&core->config)) != e_noErr ||
		(ret = get_args(core)) != e_noErr ||
		(ret = load_graphics(&core->config, &core->graphics)) != e_noErr)
		return ret;

	print_core(core);

	return e_noErr;
}