/*
** build_character_spritesheets.c for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  18:35 23/12/2020 Thomas Lombard
** Last update 18:35 23/12/2020 Thomas Lombard
*/

#include "header.h"

enum e_error parse_spritesheet_slice_spec(char **args, t_spritesheet_slice_specs *val) {
	if (tablen(args) < 5)
		return e_badArg;
	val->pos      = t_vector_from_string(args[0]);
	val->size     = t_vector_from_string(args[1]);
	val->offset   = t_vector_from_string(args[2]);
	val->center   = t_vector_from_string(args[3]);
	val->n_slices = strtoul(args[4], NULL, 10);
	return e_noErr;
}

static enum e_error handle_character_line(char **args, t_spritesheet_assets_specs_map *node) {
	if (!my_strcmp(args[0], SPEC_FILENAME))
		node->spritesheet_key = xstrdup(args[1]);
	else if (!my_strcmp(args[0], SPEC_MOVE_LEFT))
		return parse_spritesheet_slice_spec(&args[1], &node->move_left);
	else if (!my_strcmp(args[0], SPEC_MOVE_RIGHT))
		return parse_spritesheet_slice_spec(&args[1], &node->move_right);
	else if (!my_strcmp(args[0], SPEC_ATTACK_LEFT))
		return parse_spritesheet_slice_spec(&args[1], &node->attack_left);
	else if (!my_strcmp(args[0], SPEC_ATTACK_RIGHT))
		return parse_spritesheet_slice_spec(&args[1], &node->attack_right);
	else if (!my_strcmp(args[0], SPEC_JUMP_LEFT))
		return parse_spritesheet_slice_spec(&args[1], &node->jump_left);
	else if (!my_strcmp(args[0], SPEC_JUMP_RIGHT))
		return parse_spritesheet_slice_spec(&args[1], &node->jump_right);

	return e_fileCorrupt1;
}

static enum e_error build_character_spritesheet(const char *str, t_spritesheet_assets_specs_map *node) {
	int  fp;
	char *line;
	char **args;

	if ((fp      = open(str, O_RDONLY)) <= -1)
		return e_errno;
	while ((line = get_next_line(fp)) != NULL) {
		if (str_starts_with(line, "#")) {
			free(line);
			continue;
		} else if (my_strlen(line) == 0)
			continue;
		if (!(args = explode(line, ' ')))
			return e_alloc;
		handle_character_line(args, node);
		free_2(args);
	}
	return e_noErr;
}

static int filter_select_character(void *n) {
	t_string_list *node = n;

	return (str_starts_with(node->str, "player-") || str_starts_with(node->str, "enemy-"));
}

enum e_error build_character_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics) {
	t_spritesheet_assets_specs_map *sasm_node = NULL;
	enum e_error                   err;
	char                           *filepath;

	while (files) {
		if (str_starts_with(files->str, "player-") || str_starts_with(files->str, "enemy-")) {
			if (!(sasm_node = xcalloc(1, sizeof(typeof(*sasm_node)))) ||
				!(filepath = path_join(config->asset_directory, files->str, NULL)))
				return e_alloc;
			if ((err       = build_character_spritesheet(filepath, sasm_node)) != e_noErr)
				return err;
			graphics->sasm = list_push(graphics->sasm, sasm_node);
		}
		files = files->next;
	}
	list_filter(files, filter_select_character, string_list_free_node);

	return e_noErr;
}