/*
** structs.h for raytracer2 in /home/lombar_e/rendu/Infographie/raytracer2
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  Wed May 05 13:53:50 2017 Thomas Lombard
** Last update Sun May 28 09:58:47 2017 Thomas Lombard
*/

#ifndef STRUCTS_H_
# define STRUCTS_H_

# include <SFML/Graphics/Font.h>
# include <SFML/Graphics/Text.h>
# include <SFML/Graphics/Types.h>
# include <SFML/Graphics/Color.h>
# include <SFML/System/Vector2.h>
# include <SFML/System/Vector3.h>
# include <SFML/Window.h>
# include <SFML/Window/Event.h>
# include <pthread.h>
# include <stdbool.h>
# include <stdint.h>

# include "config.h"

typedef struct s_args {
	int  ac;
	char **av;
	char **ev;
} t_args;

typedef struct s_win {
	sfVector2i     size;
	sfRenderWindow *win;
	sfTexture      *tex;
	sfSprite       *spr;
	sfEvent        evt;
	sfFont         *font;
	sfText         *text;
	sfWindowStyle  style;
	int            text_print;
} t_win;

typedef struct s_vector {
	uint32_t x;
	uint32_t y;
} t_vector;

typedef struct s_config {
	char     *asset_directory;
	t_vector size;
} t_config;
/*
**	----------------------------------------
**	              Graphics structs
**	----------------------------------------
*/

typedef struct s_spritesheet_slice_specs {
	t_vector pos;
	t_vector size;
	t_vector offset;
	t_vector center;
	uint32_t n_slices;
} t_spritesheet_slice_specs;

typedef struct s_world_parallax_specs {
	struct s_world_parallax_specs *next;
	struct s_world_parallax_specs *prev;
	char                          *sprite_key;
	int                           layer_index;
	int                           speed_offset;
	t_vector                      position_offset;
} t_world_parallax_specs;

typedef struct s_world_parallax_specs_map {
	struct s_world_parallax_specs_map *next;
	struct s_world_parallax_specs_map *prev;
	char                              *key;
	struct s_world_parallax_specs     *layers;
} t_world_parallax_specs_map;

typedef struct s_spritesheet_assets_specs_map {
	struct s_spritesheet_assets_specs_map *next;
	struct s_spritesheet_assets_specs_map *prev;
	char                                  *spritesheet_key;
	struct s_spritesheet_slice_specs      move_left;
	struct s_spritesheet_slice_specs      move_right;
	struct s_spritesheet_slice_specs      jump_left;
	struct s_spritesheet_slice_specs      jump_right;
	struct s_spritesheet_slice_specs      attack_left;
	struct s_spritesheet_slice_specs      attack_right;
} t_spritesheet_assets_specs_map;

typedef struct s_texture_map {
	struct s_texture_map *next;
	struct s_texture_map *prev;
	char                 *key;
	sfTexture            *texture;
} t_texture_map;

typedef struct s_graphics {
	t_texture_map                  *texture_map;
	t_spritesheet_assets_specs_map *sasm;
	char                           **player_assets;
	t_world_parallax_specs_map     *wpsm;
} t_graphics;

/*
**	----------------------------------------
**	              Main structs
**	----------------------------------------
*/

typedef struct s_core {
	t_args     args;
	t_config   config;
	t_win      win;
	t_graphics graphics;
} t_core;

typedef struct s_fnc {
	char flag;

	int (*fnc)(t_core *, void *);
} t_fnc;

typedef struct s_string_list {
	struct s_string_list *next;
	struct s_string_list *prev;
	char                 *str;
} t_string_list;

/*
**	----------------------------------------
**	           Args parser structs
**	----------------------------------------
*/
typedef struct s_opt_func {
	char short_[10];
	char long_[20];
	int  opt_val;

	int (*fnc)(struct s_core *, int *);
} t_opt_funcs;

#endif /* ! STRUCTS_H_ */
