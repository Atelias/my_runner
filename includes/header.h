/*
** header.h for raytracer2 in /home/lombar_e/rendu/Infographie/raytracer2
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  Wed May 05 13:53:50 2017 Thomas Lombard
** Last update Thu May 31 17:28:54 2018 Thomas Lombard
*/

#ifndef HEADER_H_
# define HEADER_H_

# include "config.h"
# include "errors.h"
# include "structs.h"
# include "lib/lib.h"

/*
 *	INIT / KILL
 */
enum e_error	core_init(t_core *, int ac, char **av, char **ev);
enum e_error	core_kill(t_core *);
enum e_error 	load_graphics(t_config *config, t_graphics *graphics);
enum e_error 	build_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics);
enum e_error 	build_character_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics);
enum e_error 	build_map_spritesheets(t_string_list *files, t_config *config, t_graphics *graphics);

/*
 *	OPT_MANAGE
 */
void			arg_error(char *args[], char *arg);
enum e_error	get_args(t_core *core);
void			set_fnc_tab(t_fnc fnc[NB_FLAGS]);
void			set_fnc_tab_window_size(t_fnc fnc[NB_FLAGS], int i);
void 			set_fnc_tab_assets(t_fnc fnc[NB_FLAGS], int i);

/*
 *	PRINT
 */
void			print_core(const t_core *core);
void			print_config(const t_config *config);
void			print_graphics(const t_graphics *graphics);
void			print_spritesheet_assets_specs_map(t_spritesheet_assets_specs_map *map);
void			print_spritesheet_assets_specs_node(void *node);
void			print_spritesheet_slice_spec(t_spritesheet_slice_specs *slice_specs);
void			print_texture_map(t_texture_map *map);
void 			print_world_parallax_specs_map(t_world_parallax_specs_map *map);
void 			print_world_parallax_specs_map_node(void *node);

/*
 *	TOOLS
 */
t_vector		t_vector_from_string(char *str);
void 			string_list_free_node(void *node_to_free);

#endif /* ! HEADER_H_ */
