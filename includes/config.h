/*
** config.h for my_runner
**
** Made by Thomas Lombard
** Login   <lombar_e@epitech.net>
**
** Started on  22:47 20/12/2020 Thomas Lombard
** Last update 22:47 20/12/2020 Thomas Lombard
*/

#ifndef MY_RUNNER_CONFIG_H
# define MY_RUNNER_CONFIG_H

# define NB_FLAGS 				3
# define SPEC_FILENAME      	"FILENAME"
# define SPEC_MOVE_LEFT     	"MOVE-LEFT"
# define SPEC_MOVE_RIGHT    	"MOVE-RIGHT"
# define SPEC_JUMP_LEFT			"JUMP-LEFT"
# define SPEC_JUMP_RIGHT    	"JUMP-RIGHT"
# define SPEC_ATTACK_LEFT		"ATTACK-LEFT"
# define SPEC_ATTACK_RIGHT		"ATTACK-RIGHT"
# define SPEC_LAYER				"LAYER"
# define SPEC_SPEED_OFFSET		"SPEED_OFFSET"
# define SPEC_POSITION_OFFSET	"POSITION_OFFSET"

# if defined(OVERRIDE_DEFAULT_ASSET_DIR)
#  define DEFAULT_ASSET_DIRECTORY OVERRIDE_DEFAULT_ASSET_DIR
# else
#  define DEFAULT_ASSET_DIRECTORY "./assets"
# endif

# if defined(OVERRIDE_DEFAULT_SCREEN_WIDTH)
#  define DEFAULT_SCREEN_WIDTH OVERRIDE_DEFAULT_SCREEN_WIDTH
# else
#  define DEFAULT_SCREEN_WIDTH 1280
# endif

# if defined(OVERRIDE_DEFAULT_SCREEN_HEIGHT)
#  define DEFAULT_SCREEN_HEIGHT OVERRIDE_DEFAULT_SCREEN_HEIGHT
# else
#  define DEFAULT_SCREEN_HEIGHT 720
# endif

#endif //MY_RUNNER_CONFIG_H
